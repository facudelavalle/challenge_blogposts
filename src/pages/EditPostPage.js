import EditPost from '../components/Posts/EditPost';

const EditPostPage = () => {
  return (
    <div className='container'>
      <div className='row'>
        <div>
          <EditPost />
        </div>
      </div>
    </div>
  );
};

export default EditPostPage;
