import React, { useEffect, useState } from 'react';
import { getRequest } from '../services/requests';
import CardGrid from '../components/Card/CardGrid';

const HomePage = () => {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    const fetchPosts = async () => {
      const response = await getRequest(
        'https://jsonplaceholder.typicode.com/posts'
      );
      setPosts(response.data);
    };

    fetchPosts();
  }, []);

  return <CardGrid posts={posts} />;
};

export default HomePage;
