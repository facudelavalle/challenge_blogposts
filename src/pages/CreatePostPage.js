import PostForm from '../components/Form/PostForm';

const CreatePostPage = () => {
  return <PostForm createPost lblTitle='Title' lblBody='Content' />;
};

export default CreatePostPage;
