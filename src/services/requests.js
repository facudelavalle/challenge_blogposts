import axios from 'axios';

export const getRequest = async (url) => {
  const res = await axios.get(url);
  return res;
};

export const postRequest = async (url, post) => {
  const res = await axios.post(url, {
    userId: Math.floor(Math.random() * 100),
    id: Math.floor(Math.random() * 1000),
    title: post.title,
    body: post.body,
  });
  return res;
};

export const putRequest = async (url, post) => {
  const res = await axios.put(url, {
    userId: post.userId,
    id: post.id,
    title: post.title,
    body: post.body,
  });
  return res;
};

export const deleteRequest = async (url) => {
  const res = await axios.delete(url);
  return res;
};
