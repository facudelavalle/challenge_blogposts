import Layout from './components/Layout/Layout';
import React, { useContext } from 'react';
import AuthContext from './store/auth-context';
import { Switch, Route, Redirect } from 'react-router-dom';
import { positions, Provider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';

import AuthPage from './pages/AuthPage';
import HomePage from './pages/HomePage';
import PostPage from './pages/PostPage';
import CreatePostPage from './pages/CreatePostPage';
import EditPostPage from './pages/EditPostPage';

function App() {
  const options = {
    position: positions.BOTTOM_CENTER,
    timeout: 5000,
    offset: '30px',
  };

  const authCtx = useContext(AuthContext);

  return (
    <Provider template={AlertTemplate} {...options}>
      <Layout>
        <Switch>
          <Route path='/' exact>
            <AuthPage />
          </Route>
          {authCtx.isLoggedIn && (
            <React.Fragment>
              <Route path='/home'>
                <HomePage />
              </Route>
              <Route path='/post-detail/:postId'>
                <PostPage />
              </Route>
              <Route path='/create-post'>
                <CreatePostPage />
              </Route>

              <Route path='/edit-post/:postId'>
                <EditPostPage />
              </Route>
            </React.Fragment>
          )}
          <Route path='*'>
            <Redirect to='/' />
          </Route>
        </Switch>
      </Layout>
    </Provider>
  );
}

export default App;
