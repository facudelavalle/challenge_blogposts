import { useContext } from 'react';
import { Link } from 'react-router-dom';
import AuthContext from '../../store/auth-context';

const MainNavigation = () => {
  const authCtx = useContext(AuthContext);

  const isLoggedIn = authCtx.isLoggedIn;

  const logoutHandler = () => {
    authCtx.logout();
  };

  return (
    <header>
      <nav
        className='navbar navbar-light shadow-sm'
        style={{
          backgroundColor: 'linear-gradient(to right, #d3cce3, #e9e4f0) ',
        }}
      >
        <div className='container-fluid'>
          <div>
            {isLoggedIn && (
              <Link to='/home' style={{ textDecoration: 'underline' }}>
                <span className='navbar-brand'>Home</span>
              </Link>
            )}

            {!isLoggedIn && <h1 className='navbar-brand'> Blog Posts</h1>}

            {isLoggedIn && (
              <Link to='/create-post' style={{ textDecoration: 'underline' }}>
                <span className='navbar-brand'>Create post</span>
              </Link>
            )}
          </div>

          {isLoggedIn && (
            <button
              type='button'
              className='btn btn-outline-primary'
              onClick={logoutHandler}
            >
              Logout
            </button>
          )}
        </div>
      </nav>
    </header>
  );
};

export default MainNavigation;
