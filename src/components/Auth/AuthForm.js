import React, { useContext, useState } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { loginUser, checkTokenValidity } from '../../services/login';
import AuthContext from '../../store/auth-context';
import { useHistory } from 'react-router-dom';
import background from '../../assets/logo_alkemy.png';

const AuthForm = () => {
  const authCtx = useContext(AuthContext);
  const [tokenValidity, setTokenValidity] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const history = useHistory();
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .required('Email is required')
        .email('Enter a valid email address'),
      password: Yup.string().required('Password is required'),
    }),
    onSubmit: async (credentials) => {
      setIsLoading(true);
      const token = await loginUser(credentials);
      const isTokenValid = checkTokenValidity(token);
      setIsLoading(false);
      if (!isTokenValid) {
        setTokenValidity(false);
        return;
      }
      authCtx.login(token);
      history.replace('/home');
    },
  });

  return (
    <div className='container w-75 bg-primary mt-5 rounded shadow'>
      <div className='row align-items-stretch'>
        <div
          className='col d-none d-lg-block col-md-5 col-xl-6 rounded'
          style={{
            backgroundImage: `url(${background})`,
            backgroundPosition: 'center center',
          }}
        ></div>
        <div className='col bg-white pt-4 rounded-end '>
          <h2 className='fw-bold text-center'>Welcome</h2>
          <form onSubmit={formik.handleSubmit} className='p-5'>
            <div className='mb-4'>
              <label htmlFor='email' className='form-label'>
                Email
              </label>
              <input
                className='form-control'
                id='email'
                name='email'
                type='email'
                placeholder='Enter Email'
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.email}
              />

              {formik.touched.email && formik.errors.email ? (
                <p className='text-danger d-inline font-weight-bold'>
                  {formik.errors.email}
                </p>
              ) : null}
            </div>
            <div className='mb-4'>
              <label htmlFor='password' className='form-label'>
                Password
              </label>
              <input
                autoComplete='on'
                className='form-control'
                id='password'
                name='password'
                type='password'
                placeholder='Enter Password'
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.password}
              />
              {formik.touched.password && formik.errors.password ? (
                <p className='text-danger d-inline font-weight-bold'>
                  {formik.errors.password}
                </p>
              ) : null}
            </div>

            <div className='d-grid'>
              <button type='submit' className='btn btn-primary'>
                Login
              </button>
            </div>

            {isLoading && (
              <div className='text-center mt-5'>
                <div className='spinner-border text-dark ' role='status'></div>
              </div>
            )}

            {!tokenValidity && !isLoading && (
              <div className='mt-5 text-center text-danger font-weight-bold'>
                Email or password incorrect
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
};

export default AuthForm;
