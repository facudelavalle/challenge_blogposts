import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getRequest } from '../../services/requests';

const PostDetail = () => {
  const [post, setPost] = useState();
  const { postId } = useParams();

  const [isLoading, setIsLoading] = useState(false);
  const [errorAPI, setErrorAPI] = useState(false);

  const endpoint = `https://jsonplaceholder.typicode.com/posts/${postId}`;

  //obtengo el post actual desde la api
  useEffect(() => {
    const fetchPost = async () => {
      setErrorAPI(false);
      setIsLoading(true);

      try {
        const response = await getRequest(endpoint);
        setPost(response.data);
        setIsLoading(false);
      } catch (error) {
        setErrorAPI(true);
      }
    };

    fetchPost();
  }, [endpoint]);

  return (
    <div className='container pt-5'>
      <div className='row'>
        <div className='my-auto p-3'>
          {!post && isLoading && !errorAPI && (
            <div className='d-flex justify-content-center'>
              <div className='spinner-border text-dark' role='status'></div>
            </div>
          )}

          {errorAPI && <p className='text-center'>No results found</p>}

          {post && !isLoading && (
            <div className=''>
              <h1>{post.title}</h1>
              <p className='mt-4'>{post.body}</p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default PostDetail;
