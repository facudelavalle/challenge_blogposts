import React from 'react';
import Card from './Card';

const CardGrid = ({ posts }) => {
  return (
    <div className='container mt-5'>
      <div className='row'>
        {posts.map((post) => {
          return (
            <div className='col-lg-4 col-md-6 h-100' key={post.id}>
              <Card post={post} />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default CardGrid;
