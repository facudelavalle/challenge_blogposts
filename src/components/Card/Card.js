import React from 'react';
import { useHistory } from 'react-router-dom';
import { deleteRequest } from '../../services/requests';
import { useAlert } from 'react-alert';

const Card = ({ post }) => {
  const history = useHistory();
  const alert = useAlert();

  const endpoint = `https://jsonplaceholder.typicode.com/posts/${post.id}`;

  const clickDetailHandler = () => {
    history.push(`/post-detail/${post.id}`);
  };

  const clickEditHandler = () => {
    history.push(`/edit-post/${post.id}`);
  };

  const clickRemoveHandler = async () => {
    const res = await deleteRequest(endpoint);
    if (res.status === 200) {
      alert.success('Post deleted');
    }
  };

  return (
    <div className='shadow' style={{ height: '200px' }}>
      <div className='card h-100 mt-2 d-flex justify-content-between align-items-center'>
        <p className='text-center pt-5 px-3'>{post.title}</p>
        <div
          className='mb-4 d-flex justify-content-around'
          style={{
            width: '15rem',
          }}
        >
          <button onClick={clickDetailHandler} className='btn btn-outline-info'>
            Details
          </button>
          <button
            onClick={clickEditHandler}
            className='btn btn-outline-warning'
          >
            Edit
          </button>
          <button
            className='btn btn-outline-danger'
            onClick={clickRemoveHandler}
          >
            Remove
          </button>
        </div>
      </div>
    </div>
  );
};

export default Card;
