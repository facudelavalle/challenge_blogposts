import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { postRequest } from '../../services/requests';
import { useAlert } from 'react-alert';

const CreatePostForm = () => {
  const alert = useAlert();
  const url = `https://jsonplaceholder.typicode.com/posts`;

  const formik = useFormik({
    initialValues: {
      title: '',
      body: '',
    },
    validationSchema: Yup.object({
      title: Yup.string().required('Title is required'),
      body: Yup.string().required('Content is required'),
    }),
    onSubmit: async (post) => {
      const res = await postRequest(url, post);
      if (res.status === 201) {
        alert.success('Post created successfully');
      }
    },
  });

  return (
    <div className='container mt-5 pb-5'>
      <form onSubmit={formik.handleSubmit} className=''>
        <div className='mb-4'>
          <label htmlFor='title' className='form-label'>
            Title
          </label>
          <input
            type='text'
            id='title'
            name='title'
            placeholder='Enter Title...'
            onChange={formik.handleChange}
            value={formik.values.title}
            onBlur={formik.handleBlur}
            className='form-control'
          />
          {formik.touched.title && formik.errors.title ? (
            <p className='text-danger d-inline font-weight-bold'>
              {formik.errors.title}
            </p>
          ) : null}
        </div>
        <div className='mb-4'>
          <label htmlFor='body' className='form-label'>
            Body
          </label>
          <textarea
            placeholder='Enter content...'
            rows='10'
            cols='50'
            className='form-control'
            id='body'
            name='body'
            type='text'
            onChange={formik.handleChange}
            value={formik.values.body}
          />
          {formik.touched.body && formik.errors.body ? (
            <p className='text-danger d-inline font-weight-bold'>
              {formik.errors.body}
            </p>
          ) : null}
        </div>
        <div className='d-grid'>
          <button type='submit' className='btn btn-primary'>
            Submit
          </button>
        </div>
      </form>
    </div>
  );
};

export default CreatePostForm;
